//import express 
const express = require("express");

//import path
const path = require("path");

//khoi tao express
const app = express();

//import router
const courseRouter = require('./views/routers/courseRouter');

//import mongoose
const mongoose = require("mongoose");



//cau hinh app doc dk body
app.use(express.json());

//cau hinh ho tro tieng viet
app.use(express.urlencoded({
    extended: true
}))

//tao port
const port = 8000;

//connet mongoose
mongoose.connect('mongodb://localhost:27017/Course365', (err) => {
    if(err){
        throw err
    }
    console.log('Connect mongoDB successfully!!!');
})

app.use(express.static(__dirname +'/views/render'));

app.get('/', (req, res) => {

    res.sendFile(path.join(__dirname + '/views/render/index.html'));

});

//middleware 
// app.use( (req, res, next) => {
//     console.log("Time", new Date());
//     next();
// },
// (req, res, next) => {
//     console.log("request metho: ", req.method);
//     next();
// })

app.use('/', courseRouter);


//chay app
app.listen(port, () => {
    console.log(`App is running ${port}`);
});
