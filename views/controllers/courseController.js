//import model 
const courseModel = require('../model/courseModel');

//import mongoose
const mongoose = require("mongoose");

//=================================================
//post range
const createCourse = (req, res) => {
    //B1: thu thap du lieu
    let bodyReq = req.body;
    //B2: validate
    if(!bodyReq.courseCode){
        res.status(400).json({
            status: 'Err 400: Bad request',
            message: "courseCode is unique & required"
        })
    }
    if(!bodyReq.courseName){
        res.status(400).json({
            status: 'Err 400: Bad request',
            message: "courseName is  required"
        })
    }  
    if(!(Number.isInteger(bodyReq.price) && bodyReq.price > 0)){
        res.status(400).json({
            status: 'Err 400: Bad request',
            message: "price is  required"
        })
    } 
    if(!(Number.isInteger(bodyReq.discountPrice) && bodyReq.discountPrice > 0)){
        res.status(400).json({
            status: 'Err 400: Bad request',
            message: "price is  required"
        })
    } 
    if(!bodyReq.duration){
        res.status(400).json({
            status: 'Err 400: Bad request',
            message: "duration is  required"
        })
    }  
    if(!bodyReq.level){
        res.status(400).json({
            status: 'Err 400: Bad request',
            message: "level is  required"
        })
    }  
    if(!bodyReq.coverImage){
        res.status(400).json({
            status: 'Err 400: Bad request',
            message: "coverImage is  required"
        })
    }  
    if(!bodyReq.teacherName){
        res.status(400).json({
            status: 'Err 400: Bad request',
            message: "teacherName is  required"
        })
    }  
    if(!bodyReq.teacherPhoto){
        res.status(400).json({
            status: 'Err 400: Bad request',
            message: "teacherPhoto is  required"
        })
    }  
    //B3: thao tac CSDL
    let createCourse = {
        _id: mongoose.Types.ObjectId(),
        courseCode: bodyReq.courseCode, 
        courseName: bodyReq.courseName,
        price: bodyReq.price,
        discountPrice: bodyReq.discountPrice,
        duration: bodyReq.duration,
        level: bodyReq.level,
        coverImage: bodyReq.coverImage,
        teacherName: bodyReq.teacherName,
        teacherPhoto: bodyReq.teacherPhoto,
        isPopular: bodyReq.isPopular,
        isTrending: bodyReq.isTrending
    }

    courseModel.create(createCourse, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Isternal server error",
                message: err.message
            })
        }else{
            res.status(201).json({
                status: "Success: post voucher success!!!",
                data: data
            })
        }
    })
}

//getAll range
const getAllCourse = (req, res) => {
    //B1:thu thap du lieu
    //B2: validate
    //B3: thao tac CSDL
    courseModel.find((err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Success: getAllcourse success!!",
                data: data  
            })
        }
    })
}

//get course by Id
const getCourseById = (req, res) => {
    //B1 thu thap du lieu
    let courseId = req.params.courseId;
    //B2: validate
    if(!mongoose.Types.ObjectId.isValid(courseId)){
        res.status(400).json({
            status: "Error 400: Bad request",
            messge: "courseId is invalid"
        })
    }else{
    //B3: thao tac CSDL
        courseModel.findById(courseId, (err, data) => {
            if(err){
                res.status(500).json({
                    status: "Error 500: Internal server error",
                    message: err.message
                })
            }else{
                res.status(200).json({
                    status: "Success: get courseId success!!",
                    data: data
                })
            }
        })
    }
}

//put course by Id
const updateCourseById = (req, res) => {
    //B1 thu thap du lieu
    let courseId = req.params.courseId;
    let bodyReq = req.body;
    //validate
    if(!mongoose.Types.ObjectId.isValid(courseId)){
        res.status(400).json({
            status: 'Error 400: Bad request',
            message: "course cannot update,  courseId is invalid"
        })
    }
    //3 thao tac CSDL
    let updateCourseId = {
        courseCode: bodyReq.courseCode,
        courseName: bodyReq.courseName,
        price: bodyReq.price,
        discountPrice: bodyReq.discountPrice,
        duration: bodyReq.duration,
        level: bodyReq.level,
        coverImage: bodyReq.coverImage,
        teacherName: bodyReq.teacherName,
        teacherPhoto: bodyReq.teacherPhoto,
        isPopular: bodyReq.isPopular,
        isTrending: bodyReq.isTrending
    }

    courseModel.findByIdAndUpdate(courseId, updateCourseId, (err, data) => {
        if(err){
            res.status(500).json({
                status: 'Error 500: Internal server error',
                message: err.message
            })
        } else{
            res.status(200).json({
                status: 'Success: update success!!',
                data: data
            })
        } 
    } )
}

//delete course by Id
const deleteCourseById = (req, res) =>{
    //B1 thu thap du lieu
    let courseId = req.params.courseId;
    //B2: validate
    if(!mongoose.Types.ObjectId.isValid(courseId)){
        res.status(400).json({
            status: 'Error 400: Bad request',
            message: "courseId is invalid"
        })
    }
    //B3 thao tac CSDL
    courseModel.findByIdAndDelete(courseId, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(204).json({
                status: "Success: Delete course success!!!",
            })
        }
    })
} 

//export router
module.exports = {
    createCourse: createCourse,
    getAllCourse: getAllCourse,
    getCourseById: getCourseById,
    updateCourseById: updateCourseById,
    deleteCourseById: deleteCourseById
}