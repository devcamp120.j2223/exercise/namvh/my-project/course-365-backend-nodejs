const courseMiddle = (request, response, next) => {
    console.log(`Method: ${request.method} - URL: ${request.url} - Time: ${new Date()}`);

    next();
}

//export
module.exports = courseMiddle
