//import express
const express = require("express");

//tao Router
const router = express.Router();

// import middleware
const courseMiddle = require('../middlewares/courseMiddleware');
router.use(courseMiddle);

//import controller
const { createCourse, getAllCourse, getCourseById, updateCourseById, deleteCourseById} = require('../controllers/courseController');
const {create} = require('../model/courseModel');


router.get('/courses_365' , getAllCourse)
router.get('/courses_365/:courseId' , getCourseById)
router.post('/courses_365' , createCourse)
router.put('/courses_365/:courseId' , updateCourseById)
router.delete('/courses_365/:courseId' , deleteCourseById)

//export
module.exports = router;

